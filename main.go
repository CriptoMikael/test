package main

import (
	"fmt"
	"math"
)

func swap(x, y string) (string, string) {
	return y, x
}

func main() {
	var a, b bool
	var z int
	fmt.Println(math.Pi)
	fmt.Println(func(a, b int) int { return a + b }(3, 4))
	fmt.Println(swap("world!", "Hello"))
	fmt.Println(z, a, b)

	c, _, java, test := true, false, "no!", 1
	fmt.Println(c, java)
	fmt.Printf("Type: %T Value: %v\n", test, test)
	fmt.Printf("%v\n%q\n", java, java)

	k := 3
	fmt.Println(k)
}
